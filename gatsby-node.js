const path = require('path')

module.exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const projectTemplate = path.resolve('./src/components/ProjectDetails/ProjectDetails.tsx')
  const { data } = await graphql(`
    query ProjectSlugs {
      allContentfulProjects {
        edges {
          node {
            slug
          }
        }
      }
    }
  `)

  data.allContentfulProjects.edges.forEach(edge => {
    createPage({
      component: projectTemplate,
      path: `/project/${edge.node.slug}`,
      context: {
        slug: edge.node.slug
      }
    })
  })
}
