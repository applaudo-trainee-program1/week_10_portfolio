
module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true
  },
  extends: [
    'plugin:react/recommended',
    'standard',
    'plugin:react/jsx-runtime',
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  plugins: [
    'react',
    '@typescript-eslint'
  ],
  root: true,
  rules: {
    '@typescript-eslint/no-shadow': ['error'],
    curly: ['warn', 'multi'],
    'dot-notation': 'off',
    indent: ['error', 2],
    'max-len': ['warn', {
      code: 100,
      ignoreComments: true,
      ignoreRegExpLiterals: true,
      ignoreTrailingComments: true
    }],
    // 'no-console': ['error'],
    'no-shadow': 'off',
    quotes: ['error', 'single'],
    semi: ['error', 'always']
  },
  ignorePatterns: ['*gatsby*'],
  settings: {
    react: {
      version: 'detect'
    }
  }
};
