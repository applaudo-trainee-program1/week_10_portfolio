import * as React from 'react';
import { Link, HeadFC, PageProps } from 'gatsby';
import { Layout } from '../components';
import { StaticImage } from 'gatsby-plugin-image';
import { Routes } from '../router/routes';

const NotFoundPage: React.FC<PageProps> = () => {
  return (
    <Layout>
    <div className="flex flex-col justify-start items-center">
      <StaticImage className="w-40 my-10 invert animate-ping" src="../images/notFound.png" alt="Not found"/>
      <p className="text-pink-500">Resource not found</p>
      <Link className='bg-slate-50 bg-opacity-20 rounded-2xl p-3 my-3' to={Routes.HOME}>
        Go to Home
      </Link>
      <Link className='bg-slate-50 bg-opacity-20 rounded-2xl p-3 my-3' to={Routes.PROJECTS}>
        Go to Projects
      </Link>
      <Link className='bg-slate-50 bg-opacity-20 rounded-2xl p-3 my-3' to={Routes.ABOUT}>
        Go to About me
      </Link>
    </div>
    </Layout>
  );
};

export default NotFoundPage;

export const Head: HeadFC = () => <title>Not found</title>;
