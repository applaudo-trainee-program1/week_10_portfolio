// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from 'react';
import { StaticImage } from 'gatsby-plugin-image';
import { Layout } from '../components';
import { itemHeightVariant, itemScaleVariant } from '../utils/constants';
import { motion } from 'framer-motion';

const About = () => {
  return (
    <Layout>
      <main className='flex flex-col justify-start items-center my-10'>
        <motion.div initial={itemScaleVariant.hidden} animate={itemScaleVariant.show}>
          <StaticImage src='../images/me.webp' alt='Max Saravia Moreira'
            className='rounded-full w-40'/>
        </motion.div>
        <motion.div initial={itemHeightVariant.hidden} animate={itemHeightVariant.show}>
          <h2 className='text-3xl my-4'>Max Saravia Moreira</h2>
        </motion.div>
        <p className='tracking-widest w-4/5 my-2'>
          As a student of electronic engineering I made C projects for embedded systems with
          state pattern and layers like firmware, primitive functions & communication layer.
          Two years ago I decided to focus more on programming, instead of hardware, so I
          decided to start studying programming systems engineering.
        </p>
        <p className='tracking-widest w-4/5 my-2'>
          In order to determine which programming language I would have affinity with, I decided
          to do a diploma course in python development and web development in javascript. I was
          very attracted to web development, but there were some things that I missed from C like a
          stronger typing (this would lead me to learn typescript). Little by little I started
          learning more technologies like react and today I&apos;m focused on front-end development,
          but with some knowledge of NodeJs, express and the future view of being a full stack
          developer.
        </p>
        <a className='my-3 bg-slate-50 bg-opacity-20 rounded-2xl p-3'
          href="https://www.coderhouse.com/certificados/624626075bdd4c0019e75352"
          target="_blank" rel="noopener noreferrer">
            React course certificate (Top 10 perform)
        </a>
        <a className='my-3 bg-slate-50 bg-opacity-20 rounded-2xl p-3'
          href="https://www.coderhouse.com/certificados/630c031fad55ee0021a20990"
          target="_blank" rel="noopener noreferrer">
            Backend nodeJs course certificate (Top 10 perform)
        </a>
        <a className='my-3 bg-slate-50 bg-opacity-20 rounded-2xl p-3'
          href="/web-development-javascript.pdf"
          download>
            Javascript web development course certificate
        </a>
        <a className='my-3 bg-slate-50 bg-opacity-20 rounded-2xl p-3'
          href="/diploma-python.pdf"
          download>
            Python course certificate
        </a>
      </main>
    </Layout>
  );
};
export default About;
