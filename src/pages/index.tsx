// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from 'react';
import type { HeadFC } from 'gatsby';
import { Layout } from '../components';
import WavyText from '../components/WavyText/WavyText';
import { graphql } from 'gatsby';
import { IGatsbyImageData } from 'gatsby-plugin-image';
import TechGrid from '../components/TechGrid/TechGrid';

export const query = graphql`
query Technologies {
  allContentfulTechnologies {
    edges {
      node {
        description
        id
        image {
          gatsbyImageData(placeholder: BLURRED, formats: PNG)
        }
      }
    }
  }
}
`;

export interface EdgeNode{
  node:{
    description: string,
    id: number,
    image: {
      gatsbyImageData: IGatsbyImageData
    }
  }
}
export interface TechnologiesData {
  allContentfulTechnologies: {
    edges: Array<EdgeNode>
  }
}

export type HomeProps = {
  data: TechnologiesData
}

const Home = ({ data }: HomeProps) => {
  return (
    <Layout>
      <main className='flex justify-center items-center my-4 flex-col'>
        <div className='flex flex-col justify-center items-center my-4 bg-slate-50
        bg-opacity-10 w-3/5 py-8 rounded-2xl'>
          <WavyText text="Frontend" />
          <WavyText text="Developer" delay={0.6}/>
        </div>
        <h2 className='text-4xl my-4'>Technologies</h2>
        <TechGrid technologies={data.allContentfulTechnologies.edges} />
      </main>
    </Layout>
  );
};
export default Home;

export const Head: HeadFC = () =>
  <>
    <title>Makz dev</title>
    <meta name="description" content="List of currently known technologies" />
  </>;
