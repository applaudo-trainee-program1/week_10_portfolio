// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from 'react';
import { graphql } from 'gatsby';
import { Layout } from '../components';
import { IGatsbyImageData } from 'gatsby-plugin-image';
import ProjectCard from '../components/ProjectCard/ProjectCard';

export const query = graphql`
query Projects {
  allContentfulProjects {
    nodes {
      id
      technologyImages {
        gatsbyImageData(placeholder: BLURRED)
        id
        description
        title
      }
      slug
      gitUrl
      deploymentUrl
      title
      staticImage {
        gatsbyImageData(placeholder: BLURRED)
      }
    }
  }
}
`;

export interface ProjectEdgeNode{
  id: number,
  technologyImages: Array<{
    id: number,
    description: string,
    gatsbyImageData: IGatsbyImageData
  }>,
  staticImage: {
    gatsbyImageData: IGatsbyImageData
  },
  slug: string,
  gitUrl: string,
  deploymentUrl: string,
  title: string
}
export interface ProjectData {
  allContentfulProjects: {
    nodes: Array<ProjectEdgeNode>
  }
}

export type ProjectsProps = {
  data: ProjectData
}
const Projects = ({ data }: ProjectsProps) => {
  return (
    <Layout>
      <h2 className='text-4xl my-4'>Projects</h2>
      {data.allContentfulProjects.nodes.map((node) =>
        <ProjectCard img={node.staticImage.gatsbyImageData} title={node.title}
          technologies={node.technologyImages} deployUrl={node.deploymentUrl}
          repoUrl={node.gitUrl} key={node.id} slug={node.slug}
        />
      )}
    </Layout>
  );
};
export default Projects;
