export enum Routes {
  HOME = '/',
  ABOUT = '/about',
  PROJECTS = '/projects',
  PROJECT_DETAILS = "/project"
}
