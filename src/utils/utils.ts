import { EdgeNode } from "../pages"
import { ProjectEdgeNode } from "../pages/projects"

export const normalizeTechnologies  = (technologies:ProjectEdgeNode["technologyImages"]):EdgeNode[]  => technologies.map(tech =>{
  return({
  node:{
    id: tech.id,
    description: tech.description,
    image: {gatsbyImageData: tech.gatsbyImageData}
    }
  })
  }
)