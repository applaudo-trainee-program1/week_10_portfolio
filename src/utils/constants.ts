
export const containerOpacityVariant = {
  hidden: { opacity: 0 },
  show: {
    opacity: 1,
    transition: {
      staggerChildren: 0.3
    }
  }
};
export const itemOpacityVariant = {
  hidden: { opacity: 0 },
  show: { opacity: 1 }
};

export const itemScaleVariant = {
  hidden: { scale: 0 },
  show: { scale: 1 }
};

export const itemHeightVariant = {
  hidden:{ height: 0 },
  show: { height: "auto" }
}