import { ProjectEdgeNode } from './../../pages/projects';
import { EdgeNode } from '../../pages';
import { IGatsbyImageData } from 'gatsby-plugin-image';

export type TechGridProps = {
  technologies:   EdgeNode[] 
}
