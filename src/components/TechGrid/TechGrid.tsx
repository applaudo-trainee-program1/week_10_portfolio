// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from 'react';
import { motion } from 'framer-motion';
import { containerOpacityVariant, itemOpacityVariant } from '../../utils/constants';
import TechCard from '../TechCard/TechCard';
import { TechGridProps } from './types';

const TechGrid = ({ technologies }:TechGridProps) => {
  return (
    <motion.section className='grid grid-cols-2 md:grid-cols-3 justify-items-center gap-0.5'
      variants={containerOpacityVariant}
      initial="hidden"
      animate="show">
      {technologies
        .map(({ node }) => <TechCard key={node.id} itemVariant={itemOpacityVariant}
          title={node.description} img={node.image.gatsbyImageData}/>)}
    </motion.section>
  );
};
export default TechGrid;
