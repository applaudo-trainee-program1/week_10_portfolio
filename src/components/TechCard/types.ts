import { Variants } from 'framer-motion';
import { IGatsbyImageData } from 'gatsby-plugin-image';

export type TechCardProps = {
  img: IGatsbyImageData,
  title: string,
  itemVariant: Variants
}
