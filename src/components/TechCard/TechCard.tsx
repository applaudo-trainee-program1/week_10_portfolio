// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from 'react';
import { GatsbyImage } from 'gatsby-plugin-image';
import { TechCardProps } from './types';
import { motion } from 'framer-motion';

const TechCard = ({ img, title, itemVariant }:TechCardProps) => {
  return (
    <motion.article
      className='flex flex-col justify-center items-center my-4 bg-slate-50 bg-opacity-20
       w-2/5 rounded-2xl p-3'
      variants={itemVariant}
      whileHover={{ scale: 1.1 }}
      transition={{ type: 'spring', stiffness: 400, damping: 17 }}>
      <GatsbyImage image={img} alt={title}/>
    </motion.article>
  );
};
export default TechCard;
