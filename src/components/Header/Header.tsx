
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React, { useState } from 'react';
import { Link } from 'gatsby';
import { Navbar } from '../';
import { Routes } from '../../router/routes';
import './Header.scss';
import { StaticImage } from 'gatsby-plugin-image';

const Header = () => {
  const [menuOn, setMenuOn] = useState(false);
  const toggleMenu = () => setMenuOn(prev => !prev);
  const handleCloseMenu = () => setMenuOn(false);

  return (
    <header className='header'>
      <Link className='header__logo' to={Routes.HOME}>
        <h1 className='header__title'>Makz Dev</h1>
      </Link>
      <button className='header__button' type='button'
        onClick={toggleMenu}>
        {!menuOn &&
        <StaticImage className='header__img' src="../../images/menu.png" alt="Menu button"/>}
        {menuOn &&
        <StaticImage
          className='header__img--smaller'
          src="../../images/close.png"
          alt="Menu button"/>}
      </button>
      {menuOn &&
        <div className='header__navbar'>
          <Navbar
            handleCloseMenu={handleCloseMenu}
          />
        </div>
      }
      <div className='header__navbar--desktop'>
        <Navbar />
      </div>
    </header>
  );
};

export default Header;
