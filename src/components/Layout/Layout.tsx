import * as React from 'react';
import { Footer, Header } from '../';
import './Layout.scss';

type LayoutProps = {
  children: React.ReactNode;
}

const Layout = ({ children }:LayoutProps) => {
  return (
    <div className='layout'>
      <div className='layout__content'>
        <Header />
        <div>{children}</div>
        <Footer />
      </div>
    </div>);
};

export default Layout;
