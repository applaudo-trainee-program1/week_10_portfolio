// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from 'react';
import { StaticImage } from 'gatsby-plugin-image';

const Footer = () => {
  return (
    <footer className='flex justify-center items-center gap-4 my-10'>
      <p> Conctact me @</p>
      <a href='mailto:saravia.jonathan@live.com' target="_blank" rel="noreferrer" >
        <StaticImage
          className='w-10 invert'
          src='../../images/email.png'
          alt="makz dev's email"/>
      </a>
    </footer>
  );
};

export default Footer;
