// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from 'react';
import { motion } from 'framer-motion';
import { ProjectCardProps } from './types';
import { GatsbyImage } from 'gatsby-plugin-image';
import TechGrid from '../TechGrid/TechGrid';
import { Link } from 'gatsby';
import { normalizeTechnologies } from '../../utils/utils';
import { Routes } from '../../router/routes';
import { itemOpacityVariant } from '../../utils/constants';

const ProjectCard = ({ img, title, technologies, deployUrl, repoUrl, slug }:ProjectCardProps) => {
  const normalizedTechnologies = normalizeTechnologies(technologies);
  return (
    <article className='flex flex-col justify-center items-center'>

      <h3 className='text-3xl my-4 decoration-dotted underline'>{title}</h3>
      <Link to={`${Routes.PROJECT_DETAILS}/${slug}`}>
        <motion.div
          className='flex flex-col justify-start items-center my-4 bg-slate-50 bg-opacity-20
          rounded-2xl p-3 max-h-[12rem] sm:max-h-[20rem] md:max-h-[25rem]'
          initial={itemOpacityVariant.hidden}
          animate={itemOpacityVariant.show}
          transition={{ duration: 0.5 }}
        >
          <GatsbyImage className='flex flex-col' imgStyle={{ height: 'max-content' }}
            image={img} alt={title} />
        </motion.div>
      </Link>
      <div className='flex justify-center items-center gap-4'>
        <motion.a
          className='bg-slate-50 bg-opacity-20 rounded-2xl p-3'
          href={deployUrl}
          target="_blank"
          whileHover={{ scale: 1.05 }}
        >
          Deploy
        </motion.a>
        <motion.a
          className='bg-slate-50 bg-opacity-20 rounded-2xl p-3'
          href={repoUrl}
          target="_blank"
          whileHover={{ scale: 1.05 }}
        >
          Repository
        </motion.a>
      </div>
      <TechGrid technologies={normalizedTechnologies} />
    </article>
  );
};
export default ProjectCard;
