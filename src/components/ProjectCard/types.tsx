import { IGatsbyImageData } from 'gatsby-plugin-image';
import { ProjectEdgeNode } from '../../pages/projects';

export type ProjectCardProps = {
  img: IGatsbyImageData,
  title: string,
  technologies: ProjectEdgeNode['technologyImages'],
  deployUrl: string,
  repoUrl: string,
  slug: string
}
