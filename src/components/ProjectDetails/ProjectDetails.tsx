// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from 'react';
import { navigate, graphql } from 'gatsby';
import Layout from '../Layout/Layout';
import { GatsbyImage, IGatsbyImageData } from 'gatsby-plugin-image';
import { motion } from 'framer-motion';
import TechGrid from '../TechGrid/TechGrid';
import { normalizeTechnologies } from '../../utils/utils';
import { itemScaleVariant } from '../../utils/constants';

export const query = graphql`
  query ProjectDetails($slug: String!) {
    allContentfulProjects(filter: {slug: {eq: $slug}}) {
      edges {
        node {
          deploymentUrl
          gitUrl
          description {
            description
          }
          staticImage {
            gatsbyImageData(placeholder: BLURRED)
          }
          title
          technologyImages {
            gatsbyImageData(placeholder: BLURRED)
            id
            description
          }
        }
      }
    }
  }
`;

export interface ProjectDetailsNode{
  deploymentUrl: string,
  description: { description: string },
  gitUrl: string
  staticImage: { gatsbyImageData: IGatsbyImageData},
  technologyImages: Array<{ gatsbyImageData: IGatsbyImageData, id: number, description: string}>,
  title: string
}
export interface Edges{
  node: ProjectDetailsNode
}

export type ProjectDetailsProps = {
  data: {
    allContentfulProjects: { edges: Array<Edges> }
  }
}

const ProjectDetails = ({ data }: ProjectDetailsProps) => {
  const {
    deploymentUrl: deployUrl,
    description: { description },
    gitUrl: repoUrl,
    title,
    staticImage: { gatsbyImageData: img },
    technologyImages: technologies
  } = data.allContentfulProjects.edges[0].node;
  const normalizedTechnologies = normalizeTechnologies(technologies);
  return (
    <Layout>
      <main className='flex flex-col justify-start items-center my-4'>
        <button className='bg-slate-50 bg-opacity-20 rounded-2xl p-3 self-end'
          onClick={() => navigate(-1)}>Go back</button>
        <h3 className='text-3xl my-4 decoration-dotted underline'>{title}</h3>
        <motion.div initial={itemScaleVariant.hidden} animate={itemScaleVariant.show}>
          <GatsbyImage image={img} alt={title} />
        </motion.div>
        <h4 className='text-xl my-6'>Description</h4>
        <p className='tracking-widest w-4/5 my-2'>{description}</p>
        <div className='flex justify-center items-center gap-4'>
          <motion.a
            className='bg-slate-50 bg-opacity-20 rounded-2xl p-3'
            href={deployUrl}
            target="_blank"
            whileHover={{ scale: 1.05 }}
          >
            Deploy
          </motion.a>
          <motion.a
            className='bg-slate-50 bg-opacity-20 rounded-2xl p-3'
            href={repoUrl}
            target="_blank"
            whileHover={{ scale: 1.05 }}
          >
            Repository
          </motion.a>
        </div>
        <h4 className='text-xl my-6'>Technologies</h4>
        <TechGrid technologies={normalizedTechnologies} />
      </main>
    </Layout>
  );
};
export default ProjectDetails;
