// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from 'react';
import { motion, Variants } from 'framer-motion';

type WavyTextProps = {
  text: string;
  delay?: number;
  replay?: boolean;
  duration?: number;
}

const WavyText = ({
  text,
  delay = 0,
  duration = 0.05,
  replay = true,
  ...props
}: WavyTextProps) => {
  const letters = Array.from(text);

  const container: Variants = {
    hidden: {
      opacity: 0
    },
    visible: (i = 1) => ({
      opacity: 1,
      transition: { staggerChildren: duration, delayChildren: i * delay }
    })
  };

  const child: Variants = {
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        type: 'spring',
        damping: 12,
        stiffness: 200
      }
    },
    hidden: {
      opacity: 0,
      y: 20,
      transition: {
        type: 'spring',
        damping: 12,
        stiffness: 200
      }
    }
  };

  return (
    <motion.p
      style={{ display: 'flex', overflow: 'hidden' }}
      variants={container}
      initial="hidden"
      className="font-extrabold text-transparent  text-4xl sm:text-6xl lg:text-8xl"
      animate={replay ? 'visible' : 'hidden'}
      {...props}
    >
      {letters.map((letter, index) => (
        <motion.span key={index} variants={child} className="bg-clip-text
        bg-gradient-60 from-purple-400 to-pink-600">
          {letter === ' ' ? '\u00A0' : letter}
        </motion.span>
      ))}
    </motion.p>
  );
};

export default WavyText;
