// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from 'react';
import { Routes } from '../../router/routes';
import { NavbarProps } from './types';
import { Link } from 'gatsby';
import './Navbar.scss';
import { StaticImage } from 'gatsby-plugin-image';

const Navbar = ({ handleCloseMenu }: NavbarProps) => {
  const closeMenu = () => {
    if (handleCloseMenu) handleCloseMenu();
  };

  return (
    <nav className='navbar'>
      <Link
        className='navbar__button'
        activeClassName= 'navbar__button--current'
        onClick={closeMenu}
        to={Routes.HOME}>Home
      </Link>
      <Link
        className='navbar__button'
        activeClassName= 'navbar__button--current'
        onClick={closeMenu}
        to={Routes.ABOUT}>About me
      </Link>
      <Link
        className='navbar__button'
        activeClassName= 'navbar__button--current'
        onClick={closeMenu}
        to={Routes.PROJECTS}>Projects
      </Link>
      <a href='mailto:saravia.jonathan@live.com' target="_blank" rel="noreferrer" >
        <StaticImage
          className='w-10 invert'
          src='../../images/email.png'
          alt="makz dev's email"/>
      </a>
    </nav>
  );
};

export default Navbar;
